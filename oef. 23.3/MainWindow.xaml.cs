﻿using System;
using System.Windows;

namespace oef._23._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            txtResultaat.Text += txtAchternaam.Text;
            txtResultaat.Text += txtVoornaam.Text.PadLeft(25);
            txtResultaat.Text += txtVerdiensten.Text.PadLeft(25) + Environment.NewLine;
            txtAchternaam.Clear();
        }
    }
}
